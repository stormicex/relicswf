const fs = require('fs')
const request = require('request')
const cheerio = require('cheerio')
const beautify = require("json-beautify")


request('http://warframe.wikia.com/wiki/Void_Relic/ByRelic', {}, (e, r, b) => {
  parse(b)
})

const parse = (data) => {
  const relics = {}

  const $ = cheerio.load(data)
  $('.article-table tr').each((i, e) => {
    let tier = $(e).find('td:nth-child(1)').text().trim().toLowerCase()

    let type = $(e).find('td:nth-child(2)').text().trim()

    let common = []
    $(e).find('td:nth-child(3)').find('a').each((i, e) => {
      common.push(itemFormat($(e).text().trim()))
    })

    let uncommon = []
    $(e).find('td:nth-child(4)').find('a').each((i, e) => {
      uncommon.push(itemFormat($(e).text().trim()))
    })

    let rare = []
    $(e).find('td:nth-child(5)').find('a').each((i, e) => {
      rare.push(itemFormat($(e).text().trim()))
    })

    if (tier == '')
      return

    if (!relics[tier])
      relics[tier] = {}
    
    relics[tier][type] = {
      common,
      uncommon,
      rare
    }
  });
  fs.writeFileSync('relics.json', beautify(relics, null, 2, 100))
}

const itemFormat = (item) => {
  item = item.toLowerCase().split(' ').join('_')

  if (item.includes('neuroptics') || item.includes('systems') || item.includes('chassis'))
    item = item.replace('_blueprint', '')

  return item
}